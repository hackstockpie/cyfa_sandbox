package sandbox.cyfa.com.cyfasandbox.contacts.listeners;

import java.util.List;

import sandbox.cyfa.com.cyfasandbox.contacts.models.PhoneContact;

/**
 * Created by pie on 10/5/15.
 * Provides stubs for hooking up custom login into the contacts loading cycle
 */
public interface ContacstLoaderListener {

    /**
     * Called before contacts loading begins
     * NOTE : All UI changes done in this callback should be done within a
     * runOnUiThread(new Runnable(){}.start()) block.
     */
    public void onBeforeLoadingContacts();

    /**
     * Called when loading of contacts completes successfully
     * NOTE : All UI changes done in this callback should be done within a
     * runOnUiThread(new Runnable(){}.start()) block.
     */
    public void onCompletedLoadingContacts(List<PhoneContact> phoneContacts);


    /**
     * Called when there's an error during loading of contacts
     * NOTE : All UI changes done in this callback should be done within a
     * runOnUiThread(new Runnable(){}.start()) block.
     */
    public void onErrorLoadingContacts(Exception error);
}
