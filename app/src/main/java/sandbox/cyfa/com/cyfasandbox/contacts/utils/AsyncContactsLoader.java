package sandbox.cyfa.com.cyfasandbox.contacts.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import sandbox.cyfa.com.cyfasandbox.contacts.models.PhoneContact;
import sandbox.cyfa.com.cyfasandbox.contacts.listeners.ContacstLoaderListener;

/**
 * Created by pie on 10/5/15.
 */
public class AsyncContactsLoader {

    public static void loadContacts(final Context context, final ContacstLoaderListener contacstLoaderListener) {
        final List<PhoneContact> phoneContacts = new ArrayList<>();
        final Uri PHONE_CONTACTS_URI = ContactsContract.Contacts.CONTENT_URI;
        final Uri PHONE_NUMBERS_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        final Uri CONTACTS_EMAIL_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        final String EMAIL_ID_COLUMN_NAME = ContactsContract.CommonDataKinds.Email.CONTACT_ID;

        final String CONTACT_ID_COLUMN_NAME = ContactsContract.Contacts._ID;
        final String CONTACT_DISPLAYNAME_COLUMN_NAME = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) ? ContactsContract.Contacts.DISPLAY_NAME_PRIMARY : ContactsContract.Contacts.DISPLAY_NAME;
        final String CONTACT_HAS_PHONENUMBER_COLUMN_NAME = ContactsContract.Contacts.HAS_PHONE_NUMBER;

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    contacstLoaderListener.onBeforeLoadingContacts();

                    Cursor cursor = context.getContentResolver().query(PHONE_CONTACTS_URI, null, null, null, null);
                    if (cursor.getCount() > 0) {
                        String id, displayName, imageUri = null;
                        int phoneNumbersCount = 0;
                        while (cursor.moveToNext()) {
                            phoneNumbersCount = Integer.parseInt(cursor.getString(cursor.getColumnIndex(CONTACT_HAS_PHONENUMBER_COLUMN_NAME)));
                            if (phoneNumbersCount > 0) {

                                PhoneContact.Builder builder = new PhoneContact.Builder();
                                id = cursor.getString(cursor.getColumnIndex(CONTACT_ID_COLUMN_NAME));
                                displayName = cursor.getString(cursor.getColumnIndex(CONTACT_DISPLAYNAME_COLUMN_NAME));
                                imageUri = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));

                                builder.setId(id).setDisplayName(displayName).setThumbnailUri(imageUri);

                                Cursor numbersCursor = context.getContentResolver().query(PHONE_NUMBERS_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + "?", new String[]{id}, null);

                                if (numbersCursor != null && numbersCursor.getCount() > 0) {
                                    String phoneNumber = null;
                                    while (numbersCursor.moveToNext()) {
                                        phoneNumber = numbersCursor.getString(numbersCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                        builder.addPhoneNumber(phoneNumber);
                                    }
                                    numbersCursor.close();
                                }

                                Cursor emailsCursor = context.getContentResolver().query(CONTACTS_EMAIL_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + "?", new String[]{id}, null);
                                if (emailsCursor != null && emailsCursor.getCount() > 0) {
                                    String emailAddress = null;
                                    while (emailsCursor.moveToNext()) {
                                        emailAddress = emailsCursor.getString(emailsCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                                        Log.i("EMAIL : ",emailAddress);
                                        builder.addEmailAddress(emailAddress);
                                    }
                                    emailsCursor.close();
                                }
                                phoneContacts.add(builder.build());
                            }

                        }
                        cursor.close();

                        contacstLoaderListener.onCompletedLoadingContacts(phoneContacts);
                    }

                } catch (Exception e) {
                    contacstLoaderListener.onErrorLoadingContacts(e);
                }
            }
        }).start();
    }
}
