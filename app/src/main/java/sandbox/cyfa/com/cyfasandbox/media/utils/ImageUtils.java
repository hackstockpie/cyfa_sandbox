package sandbox.cyfa.com.cyfasandbox.media.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;

/**
 * Created by pie on 8/24/15.
 */
public class ImageUtils {

    /**
     * Encodes Bitmap into a base64 encoded string
     *
     * @param image
     * @return
     */
    public static String encodeToBase64(Bitmap image) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        byte[] bytes = outputStream.toByteArray();
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    /**
     * Decodes a base64-encoded string into a Bitmap
     *
     * @param b64encodedImage
     * @return
     */
    public static Bitmap decodeFromBase64(String b64encodedImage) {
        byte[] bytes = Base64.decode(b64encodedImage, 0);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    /**
     * Converts bitmap into a byte[]
     * @param bitmap
     * @return
     */
    public byte[] toByteArray(Bitmap bitmap) {
        int bytes = bitmap.getByteCount();
        ByteBuffer buffer = ByteBuffer.allocate(bytes);
        bitmap.copyPixelsToBuffer(buffer);

        return buffer.array();
    }
}
