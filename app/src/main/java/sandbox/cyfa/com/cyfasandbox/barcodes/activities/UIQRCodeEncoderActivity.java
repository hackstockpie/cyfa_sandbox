package sandbox.cyfa.com.cyfasandbox.barcodes.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import sandbox.cyfa.com.cyfasandbox.R;
import sandbox.cyfa.com.cyfasandbox.barcodes.utils.QRCodeEncoder;

/**
 * Created by pie on 11/10/15.
 */
public class UIQRCodeEncoderActivity extends AppCompatActivity {
    @InjectView(R.id.txt_input)
    protected EditText inputField;
    @InjectView(R.id.iv_qr_code)
    protected ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_qr_encoder_activity_layout);
        ButterKnife.inject(this);
    }

    @OnClick(R.id.btn_encode)
    public void encode() {
        QRCodeEncoder.encode(inputField.getText().toString(), 150, 150, new QRCodeEncoder.QRCodeEncoderCallbacks() {
            @Override
            public void onQRCodeGenerated(final Bitmap bitmap) {
                if(bitmap != null){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            imageView.setImageBitmap(bitmap);
                        }
                    });
                }
            }

            @Override
            public void onError(Exception error) {}
        });
    }
}
