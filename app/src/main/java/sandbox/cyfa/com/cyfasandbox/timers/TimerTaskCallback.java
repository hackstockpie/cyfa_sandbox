package sandbox.cyfa.com.cyfasandbox.timers;

/**
 * Created by pie on 10/27/15.
 */
public interface TimerTaskCallback {
    public void executeAfterDelay();
}
