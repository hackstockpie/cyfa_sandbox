package sandbox.cyfa.com.cyfasandbox.contacts.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import sandbox.cyfa.com.cyfasandbox.R;
import sandbox.cyfa.com.cyfasandbox.contacts.utils.AsyncContactsLoader;
import sandbox.cyfa.com.cyfasandbox.contacts.adapters.UIContactAdapter;
import sandbox.cyfa.com.cyfasandbox.contacts.models.PhoneContact;
import sandbox.cyfa.com.cyfasandbox.contacts.listeners.ContacstLoaderListener;
import sandbox.cyfa.com.cyfasandbox.contacts.utils.ContactsCache;
import sandbox.cyfa.com.cyfasandbox.media.listeners.RecyclerItemClickListener;

/**
 * Created by pie on 10/5/15.
 */
public class UIContactsActivity extends AppCompatActivity implements ContacstLoaderListener {
    @InjectView(R.id.txt_search_field)
    protected EditText searchField;
    @InjectView(R.id.rv_contacts)
    protected RecyclerView recyclerView;

    private ProgressDialog progressDialog;
    private UIContactAdapter adapter;
    private List<PhoneContact> userPhoneContacts = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.ui_contacts_activity_layout);
        ButterKnife.inject(this);
        this.initViews();
        this.registerEventHandlers();
    }

    private void initViews() {
        if(ContactsCache.CONTACTS != null){
            this.userPhoneContacts = ContactsCache.CONTACTS;
        }else{
            this.userPhoneContacts = Collections.EMPTY_LIST;
        }

        adapter = new UIContactAdapter(this.userPhoneContacts);
        recyclerView.setLayoutManager(new LinearLayoutManager(UIContactsActivity.this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(this.adapter);

        if(ContactsCache.CONTACTS == null){
            AsyncContactsLoader.loadContacts(UIContactsActivity.this, this);
        }
    }

    private void registerEventHandlers() {
        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                List<PhoneContact> filteredPhoneContacts = new ArrayList<PhoneContact>();
                String searchKey = s.toString().toLowerCase();
                if (!Strings.isNullOrEmpty(searchKey)) {
                    for (PhoneContact phoneContact : userPhoneContacts) {
                        if (phoneContact.getDisplayName().toLowerCase().contains(searchKey) || phoneContact.matches(searchKey)) {
                            filteredPhoneContacts.add(phoneContact);
                        }
                    }

                    adapter.setPhoneContacts(filteredPhoneContacts);
                } else {
                    adapter.setPhoneContacts(userPhoneContacts);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(UIContactsActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                PhoneContact selectedContact = userPhoneContacts.get(position);
            }

            @Override
            public void onItemLongPress(View childView, int position) {

            }
        }));
    }

    @Override
    public void onBeforeLoadingContacts() {
        /**
         * Shows a progress dialog while contacs are being loaded
         * NOTE : It's very important to invoke this code on the UIThread using runOnUiThread(new Runnable(){})
         * because loading of contacts is being done on a separate thread and in Android, if UI changes are made
         * on any thread aside the UIThread, the system crashes.
         */
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog = new ProgressDialog(UIContactsActivity.this);
                progressDialog.setTitle("Please wait");
                progressDialog.setMessage("Loading contacts...");
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);

                progressDialog.show();
            }
        });
    }

    @Override
    /**
     * Dismisses progress bar and shows loaded contacts in listview
     * NOTE : It's very important to invoke this code on the UIThread using runOnUiThread(new Runnable(){})
     * because loading of contacts is being done on a separate thread and in Android, if UI changes are made
     * on any thread aside the UIThread, the system crashes.
     */
    public void onCompletedLoadingContacts(final List<PhoneContact> phoneContacts) {
        /**
         * Store loaded contacts into cache so that loading will be done only once
         */
        ContactsCache.CONTACTS = phoneContacts;
        userPhoneContacts = phoneContacts;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                adapter.setPhoneContacts(phoneContacts);
            }
        });
    }

    @Override
    public void onErrorLoadingContacts(final Exception error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                error.printStackTrace();
            }
        });
    }
}
