package sandbox.cyfa.com.cyfasandbox.notifications.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import sandbox.cyfa.com.cyfasandbox.R;

/**
 * Created by pie on 10/12/15.
 */
public class UICustomActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.ui_custom_activity_layout);
    }
}
