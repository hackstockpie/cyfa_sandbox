package sandbox.cyfa.com.cyfasandbox.notifications.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.onesignal.OneSignal;

import org.json.JSONObject;

import sandbox.cyfa.com.cyfasandbox.R;
import sandbox.cyfa.com.cyfasandbox.notifications.utils.NotificationUtils;

/**
 * Created by pie on 10/12/15.
 */
public class UINotificationsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.ui_notifications_activity_layout);
        OneSignal.init(UINotificationsActivity.this, NotificationUtils.GCM_PROJECT_NUMBER, NotificationUtils.ONE_SIGNAL_APP_ID, new NotificationOpenedHandler());
    }

    @Override
    protected void onPause() {
        super.onPause();
        OneSignal.onPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();
        OneSignal.onResumed();
    }

    private class NotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
        @Override
        public void notificationOpened(String s, JSONObject jsonObject, boolean b) {
            Log.i("NOTF : ","NOTICATION HAS BEEN CLICKED");
            Log.i("DATA : ", jsonObject.toString());
        }
    }
}
