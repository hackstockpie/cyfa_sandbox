package sandbox.cyfa.com.cyfasandbox.timers;

/**
 * Created by pie on 02/06/2016.
 */
public class OperationExecutionTimer {

    public static long timeElapsed(TimeableOperation timeableOperation) {
        long timeBeforeExecution = System.currentTimeMillis();
        timeableOperation.executeOperation();
        long timeAfterExecution = System.currentTimeMillis();

        return timeAfterExecution - timeBeforeExecution;
    }

}
