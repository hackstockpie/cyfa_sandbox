package sandbox.cyfa.com.cyfasandbox.contacts.models;

/**
 * Created by pie on 26/04/2016.
 */
public interface Namable {
    public String getName();
}
