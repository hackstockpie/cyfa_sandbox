package sandbox.cyfa.com.cyfasandbox.notifications.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import java.util.Set;


/**
 * Created by pie on 9/11/15.
 */
public class PushNotificationService extends IntentService {
    private static final String TAG = PushNotificationService.class.getName();

    public PushNotificationService() {
        super(PushNotificationService.class.getName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        String title = extras.getString("title");
        String message = extras.getString("alert");

        Set<String> keys = extras.keySet();
        for (String key : keys) {
            Log.i(String.format("%s = ", key), extras.get(key).toString());
        }

//        showNotification(title, message);
    }

    private void showNotification(String title, String message) {
//        Intent intent = new Intent(getApplicationContext(), UINotificationActivity.class);
        Intent intent = new Intent();
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), (int) System.currentTimeMillis(), intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
        builder.setContentTitle(title)
                .setContentText(message);
//                .setSmallIcon(R.drawable.ic_launcher);
//                .setContentIntent(pendingIntent);
//                .addAction(R.drawable.logo, "Read", pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(100, builder.build());
    }
}
