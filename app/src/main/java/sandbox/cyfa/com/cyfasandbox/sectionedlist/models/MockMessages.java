package sandbox.cyfa.com.cyfasandbox.sectionedlist.models;

import java.util.ArrayList;
import java.util.List;

import sandbox.cyfa.com.cyfasandbox.sectionedlist.utils.Groupable;

/**
 * Created by pie on 29/03/2016.
 */
public class MockMessages {

    public static List<Groupable> getChatMessages() {
        List<Groupable> messages = new ArrayList<>();

        messages.add(Message.newInstance(1459720863, "This is a chat message"));
        messages.add(Message.newInstance(1459720923, "This is a chat message"));
        messages.add(Message.newInstance(1459720983, "This is a chat message"));
        messages.add(Message.newInstance(1459721043, "This is a chat message"));

        messages.add(Message.newInstance(1459807203, "This is a chat message"));
        messages.add(Message.newInstance(1459807263, "This is a chat message"));
        messages.add(Message.newInstance(1459807323, "This is a chat message"));
        messages.add(Message.newInstance(1459807383, "This is a chat message"));

        messages.add(Message.newInstance(1459893663, "This is a chat message"));
        messages.add(Message.newInstance(1459893723, "This is a chat message"));
        messages.add(Message.newInstance(1459893783, "This is a chat message"));
        messages.add(Message.newInstance(1459893843, "This is a chat message"));

        return messages;
    }
}
