package sandbox.cyfa.com.cyfasandbox.media.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import sandbox.cyfa.com.cyfasandbox.R;

public class UIVideosBrowserActivity extends AppCompatActivity{

    private Context mContext;
    private FragmentManager fragmentManager = null;
    private FragmentTransaction fragmentTransaction = null;
    private Fragment currentFragment = new sandbox.cyfa.com.cyfasandbox.media.activities.UIVideoFragment();

    private int currentPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mContext = UIVideosBrowserActivity.this;
        initialCalling();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private void initialCalling() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, currentFragment, "" + currentFragment.toString());
        fragmentTransaction.commit();
    }

}
