package sandbox.cyfa.com.cyfasandbox.photos.adapters;

import android.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import sandbox.cyfa.com.cyfasandbox.R;
import sandbox.cyfa.com.cyfasandbox.photos.models.UserAlbum;

/**
 * Created by pie on 10/28/15.
 */
public class UIUserAlbumAdapter extends RecyclerView.Adapter<UIUserAlbumAdapter.AlbumViewholder> {
    private List<UserAlbum> userAlbums;

    public UIUserAlbumAdapter(List<UserAlbum> userAlbums) {
        this.userAlbums = userAlbums;
    }

    public void setUserAlbums(List<UserAlbum> userAlbums) {
        this.userAlbums = userAlbums;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return this.userAlbums.size();
    }

    @Override
    public AlbumViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ui_user_album_cardview_layout, parent, false);
        return new AlbumViewholder(view);
    }

    @Override
    public void onBindViewHolder(AlbumViewholder holder, int position) {
        UserAlbum userAlbum = this.userAlbums.get(position);

        holder.usernameLabel.setText(String.format("%s's photos",userAlbum.getUsername()));

        List<String> photoUrls = userAlbum.getPhotoUrls();
        for (String photoUrl : photoUrls) {
            ImageView imageView = new ImageView(holder.usernameLabel.getContext());
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            LinearLayout.LayoutParams layoutParams =  new LinearLayout.LayoutParams(150, 150, 1.0f);
            layoutParams.setMargins(5,5,5,5);

            holder.photosContainer.addView(imageView,layoutParams);

            Picasso.with(imageView.getContext())
                    .load(photoUrl)
                    .placeholder(R.drawable.photo)
                    .fit()
                    .centerCrop()
                    .into(imageView);
        }

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class AlbumViewholder extends RecyclerView.ViewHolder {
        @InjectView(R.id.tv_username)
        TextView usernameLabel;
        @InjectView(R.id.ll_photos_container)
        LinearLayout photosContainer;

        public AlbumViewholder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}
