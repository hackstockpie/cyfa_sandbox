package sandbox.cyfa.com.cyfasandbox.security.utils;

import com.scottyab.aescrypt.AESCrypt;

import java.security.GeneralSecurityException;

/**
 * Created by pie on 10/23/15.
 */
public class CryptographicUtils {
    public static final String PASSWORD = "YOUR_PASSWORD_HERE";

    public static String encrypt(String plainText) throws GeneralSecurityException {
        return AESCrypt.encrypt(CryptographicUtils.PASSWORD, plainText);
    }

    public static String decrypt(String cypherText) throws GeneralSecurityException {
        return AESCrypt.decrypt(CryptographicUtils.PASSWORD, cypherText);
    }
}
