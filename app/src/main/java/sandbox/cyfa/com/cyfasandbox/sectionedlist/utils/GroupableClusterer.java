package sandbox.cyfa.com.cyfasandbox.sectionedlist.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pie on 05/04/2016.
 */
public class GroupableClusterer {

    public static Map<String, List<Groupable>> getClusters(List<Groupable> groupables) {
        Map<String, List<Groupable>> clusters = new HashMap<>();
        for (Groupable groupable : groupables) {
            if (!clusters.containsKey(groupable.getKey())) {
                clusters.put(groupable.getKey(), new ArrayList<Groupable>());
                clusters.get(groupable.getKey()).add(groupable);
            }else{
                clusters.get(groupable.getKey()).add(groupable);
            }

        }
        return clusters;
    }
}
