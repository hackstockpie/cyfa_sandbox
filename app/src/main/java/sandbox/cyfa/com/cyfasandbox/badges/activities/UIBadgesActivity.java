package sandbox.cyfa.com.cyfasandbox.badges.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import me.pie.badgify.Badgify;
import sandbox.cyfa.com.cyfasandbox.R;

/**
 * Created by pie on 24/02/2016.
 */
public class UIBadgesActivity extends AppCompatActivity {
    @InjectView(R.id.btn_show_badge)
    Button btnShowBadge;
    @InjectView(R.id.btn_clear_badge)
    Button btnClearBadge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_badges_activity_layout);

        ButterKnife.inject(this);
    }

    @OnClick(R.id.btn_show_badge)
    public void showBadge() {
        Badgify.setBadge(UIBadgesActivity.this, 28);
    }

    @OnClick(R.id.btn_clear_badge)
    public void clearBadge() {
        Badgify.removeBadge(UIBadgesActivity.this);
    }
}
