package sandbox.cyfa.com.cyfasandbox.photos.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.Collections;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import sandbox.cyfa.com.cyfasandbox.R;
import sandbox.cyfa.com.cyfasandbox.photos.adapters.UIUserAlbumAdapter;
import sandbox.cyfa.com.cyfasandbox.photos.models.UserAlbum;

/**
 * Created by pie on 10/28/15.
 */
public class UIUserAlbumsActivity extends AppCompatActivity {
    @InjectView(R.id.rv_albums)
    protected RecyclerView recyclerView;

    private List<UserAlbum> userAlbums = UserAlbum.getDummyList();
    private UIUserAlbumAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.ui_user_albums_activity_layout);
        ButterKnife.inject(this);
        this.setupAlbumsListview();
    }

    private void setupAlbumsListview() {
        this.recyclerView.setLayoutManager(new LinearLayoutManager(UIUserAlbumsActivity.this));

        this.adapter = new UIUserAlbumAdapter(userAlbums);
        this.recyclerView.setAdapter(this.adapter);
    }
}
