package sandbox.cyfa.com.cyfasandbox.timers;

import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * Created by pie on 10/27/15.
 */
public class TimerUtils {

    public static void countDown(final int targetNumber, final long tickRate, final CountdownListener countdownListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    long counter = targetNumber;
                    boolean keepCounting = true;
                    while (keepCounting) {
                        counter = counter - 1;
                        Thread.sleep(tickRate);
                        if (counter == 0) {
                            keepCounting = false;
                            if (countdownListener != null) {
                                countdownListener.onCountdownCompleted(targetNumber, tickRate);
                            }
                        }
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static void scheduleTask(final long delayInSeconds, final TimerTaskCallback timerTaskCallback) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                timerTaskCallback.executeAfterDelay();
            }
        }, delayInSeconds * 1000);
    }
}
