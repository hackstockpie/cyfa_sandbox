package sandbox.cyfa.com.cyfasandbox.notifications.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONObject;

import java.util.Set;

import de.greenrobot.event.EventBus;

/**
 * Created by pie on 10/12/15.
 */
public class OneSignalBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        EventBus eventBus = EventBus.getDefault();

        Bundle extras = intent.getBundleExtra("data");
        try {
            Set<String> keys = extras.keySet();
            for (String key : keys) {
                Log.i(String.format("%s = ", key), extras.get(key).toString());
            }

            JSONObject json = new JSONObject(extras.getString("custom"));
            Log.i("CUSTOM : ", json.toString());
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
