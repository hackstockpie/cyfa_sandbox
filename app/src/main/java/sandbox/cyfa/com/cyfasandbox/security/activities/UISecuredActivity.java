package sandbox.cyfa.com.cyfasandbox.security.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import sandbox.cyfa.com.cyfasandbox.R;
import sandbox.cyfa.com.cyfasandbox.security.utils.AppSecurityManager;

/**
 * Created by pie on 10/23/15.
 */
public class UISecuredActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppSecurityManager.setScreenCaptureAllowed(UISecuredActivity.this, false);
        this.setContentView(R.layout.ui_secured_activity_layout);
    }
}
