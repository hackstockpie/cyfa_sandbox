package sandbox.cyfa.com.cyfasandbox.utils;

import java.io.File;
import java.text.DecimalFormat;

/**
 * Created by pie on 02/06/2016.
 */
public class StorageUtils {

    public static class Converters {
        public static final int KILO_BYTES = 1024;

        public static double byteSToKB(long sizeInBytes) {
            return (double) sizeInBytes / KILO_BYTES;
        }

        public static double byteSToMB(long sizeInBytes) {
            return (double) sizeInBytes / Math.pow(KILO_BYTES, 2);
        }

        public static double byteSToGB(long sizeInBytes) {
            return (double) sizeInBytes / Math.pow(KILO_BYTES, 3);
        }

        public static double byteSToTB(long sizeInBytes) {
            return (double) sizeInBytes / Math.pow(KILO_BYTES, 4);
        }

        public static double mbToBytes(long sizeInMegaBytes) {
            return (double) sizeInMegaBytes * Math.pow(KILO_BYTES, 2);
        }

        public static String toReadableForm(long sizeInBytes) {
            if (sizeInBytes <= 0) return "0";
            final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
            int digitGroups = (int) (Math.log10(sizeInBytes) / Math.log10(1024));
            return new DecimalFormat("#,##0.#").format(sizeInBytes / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
        }
    }

    public static class Query {

        public static long getTotalSpace(File file) {
            return file.getTotalSpace();
        }

        public static long getAvailableSpace(File file) {
            return file.getFreeSpace();
        }

        public static boolean canAccomodate(long fileSizeInMegaBytes, File file) {
            return file.getFreeSpace() - StorageUtils.Converters.mbToBytes(fileSizeInMegaBytes) >= 0;
        }
    }
}
