package sandbox.cyfa.com.cyfasandbox.timers;

/**
 * Created by pie on 10/27/15.
 */
public interface CountdownListener {
    public void onCountdownCompleted(int targetNumber,long tickRate);
}
