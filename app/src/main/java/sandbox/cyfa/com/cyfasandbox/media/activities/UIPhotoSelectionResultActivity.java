package sandbox.cyfa.com.cyfasandbox.media.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import butterknife.ButterKnife;
import butterknife.InjectView;
import sandbox.cyfa.com.cyfasandbox.R;
import sandbox.cyfa.com.cyfasandbox.media.utils.PhotoPreview;

/**
 * Created by pie on 20/02/2016.
 */
public class UIPhotoSelectionResultActivity extends AppCompatActivity{
    @InjectView(R.id.iv_selected_photo_view)
    protected ImageView selectedPhotoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.ui_photo_selection_result_activity);
        ButterKnife.inject(this);

        loadImage(PhotoPreview.selectedPhotoEntry.path);
    }

    private void loadImage(String path) {
        try {
            ImageLoader.getInstance().loadImage("file://" + path, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    selectedPhotoView.setImageBitmap(loadedImage);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }
            });
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
