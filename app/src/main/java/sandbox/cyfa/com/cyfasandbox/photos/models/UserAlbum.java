package sandbox.cyfa.com.cyfasandbox.photos.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pie on 10/28/15.
 */
public class UserAlbum implements Serializable {
    private String username;
    private List<String> photoUrls;

    private UserAlbum(String username, List<String> photoUrls) {
        this.username = username;
        this.photoUrls = photoUrls;
    }

    public String getUsername() {
        return username;
    }

    public List<String> getPhotoUrls() {
        return photoUrls;
    }

    public static UserAlbum newInstance(String username, List<String> photoUrls) {
        return new UserAlbum(username, photoUrls);
    }

    public static List<UserAlbum> getDummyList() {
        List<UserAlbum> userAlbums = new ArrayList<>();

        List<String> photos1 = new ArrayList<>();
        photos1.add("http://i.forbesimg.com/media/lists/people/cristiano-ronaldo_416x416.jpg");
        photos1.add("http://i.telegraph.co.uk/multimedia/archive/03233/ronap2_3233989b.jpg");
        photos1.add("http://i.dailymail.co.uk/i/pix/2015/06/13/23/299880B800000578-3123140-image-a-1_1434234881621.jpg");
        photos1.add("http://i.telegraph.co.uk/multimedia/archive/03233/ronap2_3233989b.jpg");
        photos1.add("http://i.forbesimg.com/media/lists/people/cristiano-ronaldo_416x416.jpg");

        List<String> photos2 = new ArrayList<>();
        photos2.add("http://i.dailymail.co.uk/i/pix/2015/06/13/23/299880B800000578-3123140-image-a-1_1434234881621.jpg");
        photos2.add("http://i.telegraph.co.uk/multimedia/archive/03233/ronap2_3233989b.jpg");
        photos2.add("http://i.forbesimg.com/media/lists/people/cristiano-ronaldo_416x416.jpg");


        List<String> photos3 = new ArrayList<>();
        photos3.add("http://sporteology.com/wp-content/uploads/2015/05/SD-LionelMessi-13.jpg");
        photos3.add("http://ste.india.com/sites/default/files/2015/05/09/355363-messi.jpg");
        photos3.add("http://pthumb.lisimg.com/image/7028350/280full.jpg");

        userAlbums.add(UserAlbum.newInstance("Edward", photos1));
        userAlbums.add(UserAlbum.newInstance("Evans", photos2));
        userAlbums.add(UserAlbum.newInstance("Rosina", photos3));
        userAlbums.add(UserAlbum.newInstance("Obama", photos1));
        userAlbums.add(UserAlbum.newInstance("Kenny", photos3));


        return userAlbums;
    }
}
