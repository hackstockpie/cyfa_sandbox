package sandbox.cyfa.com.cyfasandbox.sectionedlist.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import sandbox.cyfa.com.cyfasandbox.R;
import sandbox.cyfa.com.cyfasandbox.sectionedlist.adapters.GroupedListAdapter;
import sandbox.cyfa.com.cyfasandbox.sectionedlist.models.MockMessages;
import sandbox.cyfa.com.cyfasandbox.sectionedlist.utils.Groupable;

/**
 * Created by pie on 29/03/2016.
 */
public class UIChatMessagesActivity extends AppCompatActivity {

    @InjectView(R.id.rv_messages)
    protected RecyclerView recyclerView;

    private GroupedListAdapter adapter;
    private List<Groupable> messages = MockMessages.getChatMessages();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.ui_sectioned_list_activity_layout);
        ButterKnife.inject(this);
        this.setupMessagesListAdapter();
    }

    private void setupMessagesListAdapter() {
        this.recyclerView.setLayoutManager(new LinearLayoutManager(UIChatMessagesActivity.this));

        this.adapter = new GroupedListAdapter(messages);
        this.recyclerView.setAdapter(this.adapter);
    }
}
