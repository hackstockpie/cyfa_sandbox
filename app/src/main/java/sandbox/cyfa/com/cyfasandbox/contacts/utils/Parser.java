package sandbox.cyfa.com.cyfasandbox.contacts.utils;

import org.json.JSONArray;

import sandbox.cyfa.com.cyfasandbox.contacts.models.PhoneContact;

/**
 * Created by pie on 10/5/15.
 */
public class Parser {

    /**
     * Returns all phone numbers in @phoneContact as JSONArray
     *
     * @param phoneContact
     * @return
     */
    public static JSONArray toJsonArrayObject(PhoneContact phoneContact) {
        JSONArray jsonArray = new JSONArray();
        for (String phoneNumber : phoneContact.getPhoneNumbers()) {
            jsonArray.put(phoneNumber);
        }

        return jsonArray;
    }

    /**
     * Returns all phone numbers in phoneContact as a string representation of a JSONArray
     *
     * @param phoneContact
     * @return
     */
    public static String toJsonArray(PhoneContact phoneContact) {
        return Parser.toJsonArray(phoneContact).toString();
    }
}
