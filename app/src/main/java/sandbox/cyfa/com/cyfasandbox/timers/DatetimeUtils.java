package sandbox.cyfa.com.cyfasandbox.timers;

import java.util.Date;

/**
 * Created by pie on 10/27/15.
 */
public class DatetimeUtils {

    public enum DateComparator {
        GREATER_THAN,
        LESS_THAN;
    }

    public static boolean compareDate(Date timestamp, DateComparator comparator) {
        boolean result = false;
        Date now = new Date();
        switch (comparator) {
            case GREATER_THAN:
                result = now.before(timestamp);
                break;
            case LESS_THAN:
                result = now.after(timestamp);
                break;
        }
        return result;
    }
}
