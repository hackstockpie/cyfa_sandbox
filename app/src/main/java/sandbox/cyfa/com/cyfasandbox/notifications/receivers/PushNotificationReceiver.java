package sandbox.cyfa.com.cyfasandbox.notifications.receivers;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import sandbox.cyfa.com.cyfasandbox.notifications.services.PushNotificationService;

/**
 * Created by pie on 9/11/15.
 */
public class PushNotificationReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        ComponentName componentName = new ComponentName(context.getPackageName(), PushNotificationService.class.getName());
        startWakefulService(context, (intent.setComponent(componentName)));
        setResultCode(Activity.RESULT_OK);
    }
}
