package sandbox.cyfa.com.cyfasandbox.timers;

/**
 * Created by pie on 02/06/2016.
 */
public interface TimeableOperation {
    public void executeOperation();
}
