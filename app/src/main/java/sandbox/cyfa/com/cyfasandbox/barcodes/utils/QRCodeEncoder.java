package sandbox.cyfa.com.cyfasandbox.barcodes.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by pie on 11/10/15.
 */
public class QRCodeEncoder {
    public interface QRCodeEncoderCallbacks {
        public void onQRCodeGenerated(Bitmap bitmap);

        public void onError(Exception error);
    }

    public static void encode(final String data, final int width, final int height, final QRCodeEncoderCallbacks callbacks) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final String API_ENDPOINT = "https://api.qrserver.com/v1/create-qr-code/";
                final String API_URL = String.format("%s?size=%sx%s&data=%s", API_ENDPOINT, width, height, data);

                HttpURLConnection connection = null;
                try {
                    connection = (HttpURLConnection) new URL(API_URL).openConnection();
                    final Bitmap qrCode = BitmapFactory.decodeStream(connection.getInputStream());
                    callbacks.onQRCodeGenerated(qrCode);
                } catch (Exception e) {
                    e.printStackTrace();
                    callbacks.onError(e);
                }
            }
        }).start();
    }
}
