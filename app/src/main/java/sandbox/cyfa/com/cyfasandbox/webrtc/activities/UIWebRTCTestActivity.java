package sandbox.cyfa.com.cyfasandbox.webrtc.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import butterknife.InjectView;
import butterknife.OnClick;
import sandbox.cyfa.com.cyfasandbox.R;
import sandbox.cyfa.com.cyfasandbox.webrtc.services.WebRTCService;

public class UIWebRTCTestActivity extends AppCompatActivity {
    @InjectView(R.id.btn_wrtc_start)
    protected Button btnStartService;
    @InjectView(R.id.btn_wrtc_stop)
    protected Button btnStopService;


    private WebRTCService webRTCService;
    private boolean isBound = false;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Toast.makeText(UIWebRTCTestActivity.this,"CONNECTED TO WEB RTC SERVICES",Toast.LENGTH_LONG).show();
            WebRTCService.WebRTCServiceBinder binder = (WebRTCService.WebRTCServiceBinder) service;
            webRTCService = binder.getService();
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Toast.makeText(UIWebRTCTestActivity.this,"DISCONNECTED TO WEB RTC SERVICES",Toast.LENGTH_LONG).show();
            isBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.ui_web_rtc_test_activity_layout);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent(UIWebRTCTestActivity.this, WebRTCService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(serviceConnection);
    }

    @OnClick(R.id.btn_wrtc_start)
    public void startService() {

    }

}
