package sandbox.cyfa.com.cyfasandbox.contacts.adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.common.base.Strings;

import java.io.IOException;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.hdodenhof.circleimageview.CircleImageView;
import sandbox.cyfa.com.cyfasandbox.R;
import sandbox.cyfa.com.cyfasandbox.contacts.models.PhoneContact;

/**
 * Created by pie on 10/5/15.
 */
public class UIContactAdapter extends RecyclerView.Adapter<UIContactAdapter.ContactViewholder> {
    private List<PhoneContact> phoneContacts;

    public UIContactAdapter(List<PhoneContact> phoneContacts) {
        this.phoneContacts = phoneContacts;
    }

    public void setPhoneContacts(List<PhoneContact> phoneContacts) {
        this.phoneContacts = phoneContacts;
        this.notifyDataSetChanged();
    }

    public List<PhoneContact> getPhoneContacts() {
        return phoneContacts;
    }

    @Override
    public int getItemCount() {
        return this.phoneContacts.size();
    }

    @Override
    public ContactViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ui_contact_cardview_layout, parent, false);
        return new ContactViewholder(view);
    }

    @Override
    public void onBindViewHolder(ContactViewholder holder, int position) {
        PhoneContact phoneContact = phoneContacts.get(position);

        holder.contactNameLabel.setText(phoneContact.getDisplayName());
        holder.primaryPhoneNumberLabel.setText(phoneContact.getPrimaryPhoneNumber());
        if (!Strings.isNullOrEmpty(phoneContact.getThumbnailUri())) {
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(holder.contactNameLabel.getContext().getContentResolver(), Uri.parse(phoneContact.getThumbnailUri()));
                holder.thumbnailImageview.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
                holder.thumbnailImageview.setImageResource(R.drawable.ic_contact);
            }

        } else {
            holder.thumbnailImageview.setImageResource(R.drawable.ic_contact);
        }
    }

    public static class ContactViewholder extends RecyclerView.ViewHolder {
        @InjectView(R.id.iv_contact_thumbnail)
        CircleImageView thumbnailImageview;
        @InjectView(R.id.tv_contact_displayname)
        TextView contactNameLabel;
        @InjectView(R.id.tv_contact_primarynumber)
        TextView primaryPhoneNumberLabel;

        public ContactViewholder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}
