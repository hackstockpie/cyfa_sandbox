package sandbox.cyfa.com.cyfasandbox.media.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import sandbox.cyfa.com.cyfasandbox.R;

/**
 * Created by pie on 9/22/15.
 */
public class UIMediaActivity extends AppCompatActivity {
    /*
    Request code for processing response from native image selection activity (eg. gallery, photos etc)
    Used in onActivityResult to get a handle to the image file that was selected by the user
     */
    public static final int IMAGE_CAPTURE_ACTIVITY_REQUEST_CODE = 100;
    /*
    Request code for processing response from video selection activity
    Used in onActivityResult to get a handle to the video file that was selected by the user
     */
    public static final int VIDEO_CAPTURE_ACTIVITY_REQUEST_CODE = 200;
    /*
    Passed as an extras to @UIMediaPreviewActivity to determine the type of content (image/video that was selected)
    This is important because UIMediaPreveiwActivity is used to preview both pictures and videos
     */
    public static final String MEDIA_TYPE_ID = "MEDIA_TYPE_ID";
    /*
    MEDIA_TYPE_PHOTO is the value for the MEDIA_TYPE_ID key passed to UIMediaPreviewActivity when an image is selected
     */
    public static final int MEDIA_TYPE_PHOTO = 1;
    /*
    MEDIA_TYPE_VIDEO is the value for the MEDIA_TYPE_ID key passed to UIMediaPreviewActivity when a video is selected
     */
    public static final int MEDIA_TYPE_VIDEO = 2;
    /*
    CAPTURED_PHOTO_DATA is the name of the key used to pass the photo (as a Bitmap) taken with the device's camera to UIMediaPreviewActivity
    for previewing.
     */
    public static final String CAPTURED_PHOTO_DATA = "CAPTURED_PHOTO_DATA";
    /*
    CAPTURED_VIDEO_URI is the key for passing the url to the video captured to UIMediaPreviewActivity for previewing
     */
    public static final String CAPTURED_VIDEO_URI = "CAPTURED_VIDEO_URI";

    /*
    FILE_BROWSER_ROOT_DIRECTORY is the key used to pass the root directory from which UIFileBrowserActivity should begin traversing
    Values are Environment.DIRECTORY_DCIM or Environment.DIRECTORY_MOVIES
     */
    public static final String FILE_BROWSER_ROOT_DIRECTORY = "FILE_BROWSER_ROOT_DIRECTORY";
    /*
    PHOTOS_ROOT_DIRECTORY is the name of the system's designated directory for storing photos
     */
    public static final String PHOTOS_ROOT_DIRECTORY = Environment.DIRECTORY_DCIM;
    /*
    VIDEOS_ROOT_DIRECTORY is the name of the system's designated directory for storing videos
     */
    public static final String VIDEOS_ROOT_DIRECTORY = Environment.DIRECTORY_MOVIES;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.ui_media_activity_layout);
        ButterKnife.inject(this);
    }

    /**
     * Starts apps which can be used to take still photos
     */
    @OnClick(R.id.btn_native_camera)
    public void startStillCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, UIMediaActivity.IMAGE_CAPTURE_ACTIVITY_REQUEST_CODE);
        }
    }

    /**
     * Starts apps which can be used to take motion videos
     */
    @OnClick(R.id.btn_native_vcamera)
    public void startVideoCamera() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, UIMediaActivity.VIDEO_CAPTURE_ACTIVITY_REQUEST_CODE);
        }
    }

    /**
     * Starts UIFileBrowserActivity for selecting photos
     */
    @OnClick(R.id.btn_imgs_browser)
    public void showImageBrowser() {
        Intent intent = new Intent(UIMediaActivity.this, UIPhotosBrowserActivity.class);
        intent.putExtra(UIMediaActivity.FILE_BROWSER_ROOT_DIRECTORY, UIMediaActivity.PHOTOS_ROOT_DIRECTORY);
        startActivity(intent);
    }

    /**
     * Starts UIFileBrowserActivity for selecting videos
     */
    @OnClick(R.id.btn_vids_browser)
    public void showVideoBrowser() {
        Intent intent = new Intent(UIMediaActivity.this, UIVideosBrowserActivity.class);
        intent.putExtra(UIMediaActivity.FILE_BROWSER_ROOT_DIRECTORY, UIMediaActivity.VIDEOS_ROOT_DIRECTORY);
        startActivity(intent);
    }

//    /**
//     * Called when the user is done selecting either an image or photo using the systems photo/video browser activity
//     * @param requestCode
//     * @param resultCode
//     * @param data
//     */
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK) {
//            Intent previewIntent = new Intent(UIMediaActivity.this, UIMediaPreviewActivity.class);
//            switch (requestCode) {
//                /**
//                 * Called when user successfully takes a photo with the native camera app
//                 * Passes both MEDIA_TYPE_ID and CAPTURED_PHOTO_DATA to UIMediaPreviewActivity
//                 */
//                case UIMediaActivity.IMAGE_CAPTURE_ACTIVITY_REQUEST_CODE:
//                    Bundle extras = data.getExtras();
//                    Bitmap photo = (Bitmap) extras.get("data");
//
//                    previewIntent.putExtra(UIMediaActivity.MEDIA_TYPE_ID, UIMediaActivity.MEDIA_TYPE_PHOTO);
//                    previewIntent.putExtra(UIMediaActivity.CAPTURED_PHOTO_DATA, photo);
//                    break;
//                /**
//                 * Called when user successfully takes a video with the native camera app
//                 * Passes both MEDIA_TYPE_ID and CAPTURED_VIDEO_URI to UIMediaPreviewActivity
//                 */
//                case UIMediaActivity.VIDEO_CAPTURE_ACTIVITY_REQUEST_CODE:
//                    Uri videoUri = data.getData();
//
//                    previewIntent.putExtra(UIMediaActivity.MEDIA_TYPE_ID, UIMediaActivity.MEDIA_TYPE_VIDEO);
//                    previewIntent.putExtra(UIMediaActivity.CAPTURED_VIDEO_URI, videoUri);
//                    break;
//            }
//
//            /**
//             * Starts UIMediaPreviewActivity
//             */
//            startActivity(previewIntent);
//        }
//    }
}
