package sandbox.cyfa.com.cyfasandbox.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Date;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.OnClick;
import me.pie.badgify.Badgify;
import sandbox.cyfa.com.cyfasandbox.R;
import sandbox.cyfa.com.cyfasandbox.badges.activities.UIBadgesActivity;
import sandbox.cyfa.com.cyfasandbox.barcodes.activities.UIQRCodeEncoderActivity;
import sandbox.cyfa.com.cyfasandbox.contacts.activities.UIContactsActivity;
import sandbox.cyfa.com.cyfasandbox.emoji.activities.UIEmojiActivity;
import sandbox.cyfa.com.cyfasandbox.media.activities.UIMediaActivity;
import sandbox.cyfa.com.cyfasandbox.notifications.activities.UINotificationsActivity;
import sandbox.cyfa.com.cyfasandbox.photos.activities.UIUserAlbumsActivity;
import sandbox.cyfa.com.cyfasandbox.sectionedlist.activities.UIChatMessagesActivity;
import sandbox.cyfa.com.cyfasandbox.sectionedlist.models.MockMessages;
import sandbox.cyfa.com.cyfasandbox.sectionedlist.utils.Groupable;
import sandbox.cyfa.com.cyfasandbox.sectionedlist.utils.GroupableClusterer;
import sandbox.cyfa.com.cyfasandbox.security.activities.UISecuredActivity;
import sandbox.cyfa.com.cyfasandbox.timers.TimerTaskCallback;
import sandbox.cyfa.com.cyfasandbox.timers.TimerUtils;
import sandbox.cyfa.com.cyfasandbox.webrtc.activities.UIWebRTCTestActivity;

/**
 * Created by pie on 10/5/15.
 */
public class UIDemoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.ui_demo_activity_layout);
        ButterKnife.inject(this);
    }


    @OnClick(R.id.btn_chat_messages)
    public void showChatMessages(){
//        startActivity(new Intent(UIDemoActivity.this, UIChatMessagesActivity.class));
//        Map<String,List<Groupable>> map = GroupableClusterer.getClusters(MockMessages.getChatMessages());
//        for(String key: map.keySet()){
//            Log.i("KEY=",key);
//            List<Groupable> groupables = map.get(key);
//            for(Groupable groupable : groupables){
//                Log.i("MESSAGE",groupable.getBody());
//            }
//        }

        Date d = new Date();
        Log.i("DATE",d.toString());
    }

    @OnClick(R.id.btn_media)
    public void startMediaDemo() {
        startActivity(new Intent(UIDemoActivity.this, UIMediaActivity.class));
    }

    @OnClick(R.id.btn_contacts)
    public void startContactsDemo() {
        startActivity(new Intent(UIDemoActivity.this, UIContactsActivity.class));
    }

    @OnClick(R.id.btn_notifications)
    public void startNotificationsDemo() {
        startActivity(new Intent(UIDemoActivity.this, UINotificationsActivity.class));
    }

    @OnClick(R.id.btn_security)
    public void startSecurityDemo() {
        startActivity(new Intent(UIDemoActivity.this, UISecuredActivity.class));
    }

    @OnClick(R.id.btn_countdown)
    public void startTimerDemo() {
        TimerUtils.scheduleTask(5, new TimerTaskCallback() {
            @Override
            public void executeAfterDelay() {
                final String message = "WHATH NONSENSE!";
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(UIDemoActivity.this, message, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    @OnClick(R.id.btn_user_albums)
    public void showUserAlbumList() {
        startActivity(new Intent(UIDemoActivity.this, UIUserAlbumsActivity.class));
    }

    @OnClick(R.id.btn_emoji_keyboard)
    public void showEmojiActivity() {
        startActivity(new Intent(UIDemoActivity.this, UIEmojiActivity.class));
    }

    @OnClick(R.id.btn_bcode_encoder)
    public void showEncoder() {
        startActivity(new Intent(UIDemoActivity.this, UIQRCodeEncoderActivity.class));
    }

    @OnClick(R.id.btn_webrtc_demo)
    public void showWRTCDemo() {
        startActivity(new Intent(UIDemoActivity.this, UIWebRTCTestActivity.class));
    }

    @OnClick(R.id.btn_badges_demo)
    public void showBadgesDemo() {
        startActivity(new Intent(UIDemoActivity.this, UIBadgesActivity.class));
    }

    @OnClick(R.id.btn_bcode_scanner)
    public void scanQRCode() {
        IntentIntegrator integrator = new IntentIntegrator(UIDemoActivity.this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setPrompt("Scan a barcode");
        integrator.setBarcodeImageEnabled(true);
//        integrator.initiateScan();

        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, IntentIntegrator.REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case IntentIntegrator.REQUEST_CODE:
                    IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                    Log.i("QR CODE : ", intentResult.toString());

                    break;
            }
        }
    }
}
