package sandbox.cyfa.com.cyfasandbox.webrtc.services;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import me.pie.badgify.Badgify;

/**
 * Created by pie on 25/02/2016.
 */
public class FirebaseService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread(firebaseThread).start();
        return Service.START_STICKY;
    }

    Runnable firebaseThread = new Runnable() {
        @Override
        public void run() {
            Firebase.setAndroidContext(FirebaseService.this);

            Firebase firebaseRef = new Firebase("https://webrtcservice.firebaseio.com/");
            firebaseRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    int badgeCount = dataSnapshot.getValue(int.class);
                    Badgify.setBadge(FirebaseService.this, badgeCount);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
