package sandbox.cyfa.com.cyfasandbox.webrtc.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by pie on 20/02/2016.
 */
public class WebRTCService extends Service {
    private final IBinder binder = new WebRTCServiceBinder();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class WebRTCServiceBinder extends Binder {

        public WebRTCService getService() {
            return WebRTCService.this;
        }
    }
}
