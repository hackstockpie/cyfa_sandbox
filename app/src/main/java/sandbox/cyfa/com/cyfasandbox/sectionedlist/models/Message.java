package sandbox.cyfa.com.cyfasandbox.sectionedlist.models;

import java.util.Calendar;
import java.util.Date;

import sandbox.cyfa.com.cyfasandbox.sectionedlist.utils.Groupable;

/**
 * Created by pie on 29/03/2016.
 */
public class Message implements Groupable {
    private long timestamp;
    private String messageBody;
    private int day;
    private int month;
    private int year;


    private Message(long timestamp, String messageBody) {
        this.timestamp = timestamp;
        this.messageBody = messageBody;

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(this.timestamp));

        this.day = calendar.get(Calendar.DAY_OF_YEAR);
        this.month = calendar.get(Calendar.MONTH);
        this.year = calendar.get(Calendar.YEAR);
    }

    @Override
    public int getDay() {
        return day;
    }

    @Override
    public int getMonth() {
        return month;
    }

    @Override
    public int getYear() {
        return year;
    }

    @Override
    public String getKey() {
        return String.format("%s/%s/%s", this.day, this.month, this.year);
    }

    public String getContent() {
        return this.messageBody;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String getBody() {
        return messageBody;
    }

    public static Message newInstance(long timestamp, String messageBody) {
        return new Message(timestamp, messageBody);
    }
}
