package sandbox.cyfa.com.cyfasandbox.sectionedlist.utils;

import java.util.Date;

/**
 * Created by pie on 29/03/2016.
 */
public interface Groupable {
    public int getDay();
    public int getMonth();
    public int getYear();
    public String getKey();
    public String getBody();
}
