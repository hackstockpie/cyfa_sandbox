package sandbox.cyfa.com.cyfasandbox.contacts.utils;

import java.util.List;
import java.util.Map;

import sandbox.cyfa.com.cyfasandbox.contacts.ContactsNotLoadedException;
import sandbox.cyfa.com.cyfasandbox.contacts.models.PhoneContact;

/**
 * Created by pie on 10/6/15.
 */
public class ContactsCache {
    /**
     * Caches already loaded contacts so that loading wouldn't happen every time activity is called
     */
    public static List<PhoneContact> CONTACTS = null;


    /**
     * Returns a phonecontact object which matches the search key
     *
     * @param searchKey
     * @return
     * @throws ContactsNotLoadedException
     */
    public static PhoneContact find(String searchKey) throws ContactsNotLoadedException {
        if (ContactsCache.CONTACTS == null) {
            throw new ContactsNotLoadedException();
        } else {
            for (PhoneContact phoneContact : ContactsCache.CONTACTS) {
                if (phoneContact.matches(searchKey) || phoneContact.getDisplayName().toLowerCase().contains(searchKey.toLowerCase())) {
                    return phoneContact;
                }
            }
        }
        return null;
    }
}
