package sandbox.cyfa.com.cyfasandbox.sectionedlist.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import sandbox.cyfa.com.cyfasandbox.R;
import sandbox.cyfa.com.cyfasandbox.sectionedlist.utils.Groupable;

/**
 * Created by pie on 29/03/2016.
 */
public class GroupedListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Groupable> messages;
    private final int HEADER = 0, CHAT_MESSAGE = 1;

    public GroupedListAdapter(List<Groupable> messages) {
        this.messages = messages;
    }

    @Override
    public int getItemCount() {
        return this.messages.size();
    }

    @Override
    public int getItemViewType(int position) {
        return  CHAT_MESSAGE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View view = null;

        switch (viewType){
            case HEADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ui_chat_header_item,parent,false);
                viewHolder = new HeaderViewHolder(view);
                break;
            case CHAT_MESSAGE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ui_chat_message_item,parent,false);
                viewHolder = new MessageViewHolder(view);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Groupable message = this.messages.get(position);

        switch (holder.getItemViewType()){
            case HEADER:
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                headerViewHolder.headerLabel.setText(message.getBody());
                break;
            case CHAT_MESSAGE:
                MessageViewHolder messageViewHolder = (MessageViewHolder) holder;
                messageViewHolder.messageLabel.setText(message.getBody());
                break;
        }
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.tv_chat_header)
        TextView headerLabel;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.tv_chat_message)
        TextView messageLabel;

        public MessageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}
