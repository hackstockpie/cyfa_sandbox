package sandbox.cyfa.com.cyfasandbox.contacts;

/**
 * Created by pie on 10/8/15.
 */
public class ContactsNotLoadedException extends Exception {

    public ContactsNotLoadedException() {
        super("Contacts haven't been loaded yet. Invoke AsyncContactsLoader.loadContacts() before calling this function");
    }
}
